/**************************************************************************************************/
/*                                                                                                */
/*                          Think Embedded Pvt. Ltd. (TEPL) Design Unit                           */
/*                                                                                                */
/**************************************************************************************************/
/*                                                                                                */
/* All Rights Reserved Copyright (C) 2018, Think Embedded Pvt. Ltd. (TEPL)                        */
/*                                                                                                */
/* Redistribution and use in source and binary forms, with or without modification,               */
/* are not permitted in any case                                                                  */
/*                                                                                                */
/* TEPL's name may not be used to endorse or promote products derived from this software without  */
/* specific prior written permission.                                                             */
/*                                                                                                */
/**************************************************************************************************/

/**************************************************************************************************/
/*  File Name   : TELCLIENT.h                                                                     */
/*  Author      : Prasad                                                                          */
/*  Version 	: v1.0                                                                            */
/*  Date        : 19-03-2019                                                                      */
/*                                                                                                */
/* @Description : TELINKBUS Frame Work                                                            */
/**************************************************************************************************/

#ifndef _TELCLIENT_H_
#define _TELCLIENT_H_

/**************************************************************************************************/
/* System Header Files Section                                                                    */
/**************************************************************************************************/

#include <stdint.h>

/**************************************************************************************************/
/* User Header Files Section                                                                      */
/**************************************************************************************************/

#include "Includes.h"

/**************************************************************************************************/
/* Export Global Definitions Section                                                              */
/**************************************************************************************************/

#define TELCLIENT_RX_CHECKSUM				    ( 0x01 )
#define TELCLIENT_TX_CHECKSUM				    ( 0x02 )

/* TBUS Offset Definitions */
#define TELCLIENT_START_PREAMBLE_OFFSET		( 0x00 )
#define TELCLIENT_LENGTH_OFFSET				( 0x01 )
#define TELCLIENT_SID_OFFSET					( 0x05 )
#define TELCLIENT_DID_OFFSET					( 0x07 )
#define TELCLIENT_DATA_OFFSET					( 0x09 )
#define TELCLIENT_CHECSUM_OFFSET				( 0x0A )
#define TELCLIENT_ENDPREAMBLE1_OFFSET			( 0x0E )
#define TELCLIENT_ENDPREAMBLE2_OFFSET			( 0x0F )

#define TELCLIENT_START_PREAMBLE				( 0x3A )
#define TELCLIENT_END_PREAMBLE1				( 0x0D )
#define TELCLIENT_END_PREAMBLE2				( 0x0A )

#define TELCLIENT_ACK							( 0x06 )
#define TELCLIENT_NACK 					    ( 0x15 )

/* TELCBUS Service Structure */
typedef struct
{
    INT8U u8_mSID;	
    INT8U u8_mDID;	
    void (*fp_mServiceHandler)(INT8U u8_fDID, INT8U * p_fPayLoad, INT16U u16_fPayLoadLength);
    
}ST_TELCLIENT_SERVICES_t;

/* Struct For Update Meta Data */
typedef struct
{
    INT64U u64_mCheckSum;
    INT32U u32_mFileSize;
    INT32U u32_mPort;
    INT8U au8_mUpdateType[20];
    INT8U au8_mIpAddr[20];
    INT8U au8_mUserName[20];
    INT8U au8_mPassWord[20];
    INT8U au8_mFilePath[20];
    INT8U au8_mFileName[20];
}ST_UPDATE_META_DATA_t;

/* */
struct FtpFile 
{
  const char *filename;
  FILE *stream;
};

/**************************************************************************************************/
/* Export Global Variables Section                                                                */
/**************************************************************************************************/

extern INT8S *ps8_gVehicleId;

/**************************************************************************************************/
/* Export Functions Section                                                                       */
/**************************************************************************************************/

INT8U TeLClient_CheckSum(INT8U * p_fString, INT16U u16_fLength, INT8U u8_fTxRXFlag);

void TBUS_ResetApp(INT8U u8_fDID, INT8U * p_fPayLoad, INT16U u16_fPayLoadLength);

void TBUS_EnterBootLoader(INT8U u8_fDID, INT8U * p_fPayLoad, INT16U u16_fPayLoadLength);

void TBUS_EraseFlash(INT8U u8_fDID, INT8U * p_fPayLoad, INT16U u16_fPayLoadLength);

void TBUS_RequestDownload(INT8U u8_fDID, INT8U * p_fPayLoad, INT16U u16_fPayLoadLength);

void TBUS_TransferData(INT8U u8_fDID, INT8U * p_fPayLoad, INT16U u16_fPayLoadLength);

void TBUS_TransferCompleat(INT8U u8_fDID, INT8U * p_fPayLoad, INT16U u16_fPayLoadLength);

void TBUS_DeviceReset(INT8U u8_fDID, INT8U * p_fPayLoad, INT16U u16_fPayLoadLength);

INT8U TeLClient_FlashAux(void);

extern int TCreateSemaphore(int Sem_Id, key_t KeyValue, int SemValue);

extern int Semaphore_Take(int Sem_Id);

extern int Semaphore_Give(int Sem_Id);	 

extern void TeLClient_Init(void);

extern void TeLClient_GetSocketInfo(INT8U *pu8_fPubTopicRef);

extern INT8U TeLClient_RegisterHandler(INT8U u8_fSID, INT8U u8_fDID, \
											  void (*fp_fServiceHandler)(INT8U, INT8U *, INT16U));

extern void TeLClient_ProcessCmd(INT32S s32_fSocket, INT8U *pu8_fData, INT16U u16_fLength);

extern void TeLClient_SendCommand(INT32S s32_fSocket, INT8U u8_fSID, INT8U u8_fDID, INT8U * p_fData, \
																				INT16U u16_fDataLen);
extern void TeLClient_UpdateInit(INT8U *pu8_fUpdateData, INT16S s16_fDataLen);

extern INT8U TeLink_UpdateMetaData(INT8U *u8_fPayLoad, INT16U u16_fDataLength, \
                                        ST_UPDATE_META_DATA_t *st_fUpdateMetaData);

static int Ftp_GetFileFromServer(char* ServerIp, uint16_t PortNo, char* UserName, \
								char* PassWord, char* CWDirectory, char* FileName, const char* LocalFileName);

static void TECHM_RegisterHandlers(void);

#endif

/**************************************************************************************************/
/* End of  TELINKBUS.h                                                                            */
/**************************************************************************************************/
