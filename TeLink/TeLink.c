#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <netinet/in.h> 
#include <sys/socket.h>
#include <sys/types.h> 
#include <sys/ipc.h>
#include <sys/msg.h>

#include "Includes.h"

#include "MQTT/MQTT.h"
#include "TeLClient/TeLClient.h"
#include "WatsonClient/WatsonClient.h"
#include "TeLink.h"


void TELINK_PingHandler(INT8U u8_fDid, INT8U *pu8_fData, INT16U u16_fLength);

/* */
pthread_t pTeLinkHandle;

/* Connection manager state variable */
EN_TELINK_STATES_t en_gTeLinkState = TELINK_MQTT_INIT;

volatile INT8U u8_gTeLinkConnStatus = 0;
INT32S s32_gTeLinkSocketId = 0;
INT32S s32_gTeLinkCsq = 0;

INT8S *ps8_gPubTopic = "pub/TELINK4G2000005";
INT8S *ps8_gSubTopic = "sub/TELINK4G2000005";
INT8S *ps8_gUpdateTopic = "update/TELINK4G2000005";
INT8S *ps8_gParamTopic = "param/TELINK4G2000005";
INT8S *ps8_gAlertTopic = "alert/TELINK4G2000005";
INT8U au8_gSerialNo[16] = "TELINK4G2000005";

INT8U au8_gTeLClientMqttBuff[1024] = {0, };
INT16S s16_gTeLClientMqttLen = 0;

INT8U au8_gTeLClientRxTopic[50] = {0, };
INT8U au8_gTeLClinetTxTopicLen = 0;

INT8U au8_gTeLClientRxBuff[1024] = {0, };
INT16S s16_TeLClientRxLen = 0;

/* */
#define TELINK_MSG_QUEUE_KEY      (key_t)200

/* Parameters To get Key For Message Queue*/
#define TELINK_QUEUE_PATH_NAME "/tmp/msgkey2"
#define TELINK_PROJECT_ID 'T'

/* Queue for TeLClient Service Listner */
int TeLinkMsgQueueID = 0;
int TeLinkMsgQBytes = 0;
key_t TeLinkMsgQueueKey = 0;
int TeLinkMsgQFlags = 0;

/* */
void TELINK_ConnectedCb(INT32S s32_fCsq)
{
	s32_gTeLinkCsq = s32_fCsq;

	u8_gTeLinkConnStatus = 1;
}

/* */
void TELINK_DisconnectedCb(INT32S s32_fCsq)
{
	s32_gTeLinkCsq = s32_fCsq;
	u8_gTeLinkConnStatus = 0;
	en_gTeLinkState = TELINK_MQTT_INIT;
}

/* */
void TELINK_DataCallBack(INT8S *ps8_fData, INT16U u16_fLength)
{
	ST_TELINK_MSG_QUEUE_t st_lTeLinkTxMsgQueue;

	memset(&st_lTeLinkTxMsgQueue, 0x00, sizeof(st_lTeLinkTxMsgQueue));

	st_lTeLinkTxMsgQueue.Msg_type = 1;
	memcpy(st_lTeLinkTxMsgQueue.QueueBuff, ps8_fData, u16_fLength);
	st_lTeLinkTxMsgQueue.MsgLength = u16_fLength;

	msgsnd(TeLinkMsgQueueID, &st_lTeLinkTxMsgQueue, sizeof(st_lTeLinkTxMsgQueue), 0);
}

/* TeLClient thread */
void *pTELINK_ConnMgrThread(void *vp_fParams)
{
	fd_set fdset;
	struct timeval timeout = {0, 500000};

	ST_TELINK_MSG_QUEUE_t st_lTeLinkRxMsgQueue;

	st_lTeLinkRxMsgQueue.Msg_type = 1;

	for(;;)
	{
		switch(en_gTeLinkState)
		{
		case TELINK_MQTT_INIT:
		{
			u8_gWClientConnStatus = 0;
			if((1 == u8_gTeLinkConnStatus) && (s32_gTeLinkCsq >= 9) && (s32_gTeLinkCsq <= 31))
			{
				/* Open Tcp socket for Mqtt connection */
				s32_gTeLinkSocketId = MQTT_ClientInit("207.148.68.85", 1883);
				if(-1 == s32_gTeLinkSocketId)
				{
#ifdef __DEBUG
					printf("Socket not Created\r\n");
#endif
					sleep(5);
				}
				else
				{
#ifdef __DEBUG
					printf("Socket Created\r\n");
#endif
					/* update Conn mgr state */
					en_gTeLinkState = TELINK_MQTT_DISCONNECTED;
				}
			}
			else
			{
				/* No Action */
			}
		}
		break;

		case TELINK_MQTT_CONNECTED:
		{
			/* Read All Available Messages */
			while(1)
			{
				TeLinkMsgQBytes = msgrcv(TeLinkMsgQueueID, &st_lTeLinkRxMsgQueue, sizeof(st_lTeLinkRxMsgQueue), 1, IPC_NOWAIT);

				if(-1 == TeLinkMsgQBytes)
				{
					break;
				}
				else
				{
					/* pubish data to mqtt server */
					if(MQTT_Publish(s32_gTeLinkSocketId, ps8_gParamTopic, \
													st_lTeLinkRxMsgQueue.QueueBuff, st_lTeLinkRxMsgQueue.MsgLength) > 0)
					{
#ifdef __DEBUG
						printf("publishing to TeLink Server successful\r\n");
#endif 
					}
					else
					{
						en_gTeLinkState = TELINK_MQTT_INIT;
						break;
					}

					usleep(250000);
				}
			}
			
			memset(au8_gTeLClientMqttBuff, 0, sizeof(au8_gTeLClientMqttBuff));

			s16_gTeLClientMqttLen = recv(s32_gTeLinkSocketId, au8_gTeLClientMqttBuff, \
														sizeof(au8_gTeLClientMqttBuff), MSG_DONTWAIT);

			if(s16_gTeLClientMqttLen > 0)
			{
				memset(au8_gTeLClientRxTopic, 0, sizeof(au8_gTeLClientRxTopic));
				memset(au8_gTeLClientRxBuff, 0, sizeof(au8_gTeLClientRxBuff));
				au8_gTeLClinetTxTopicLen = 0;
				s16_TeLClientRxLen = 0;

				/* Parse Mqtt data */
				MQTT_ReadData(au8_gTeLClientMqttBuff, s16_gTeLClientMqttLen, au8_gTeLClientRxTopic, \
							&au8_gTeLClinetTxTopicLen, au8_gTeLClientRxBuff, &s16_TeLClientRxLen);

				if(strstr((const char *)au8_gTeLClientRxTopic, "sub"))
				{
					TeLClient_ProcessCmd(s32_gTeLinkSocketId , au8_gTeLClientRxBuff, s16_TeLClientRxLen);
				}
				else if(strstr((const char *)au8_gTeLClientRxTopic, "update"))
				{
					TeLClient_UpdateInit(au8_gTeLClientRxBuff, s16_TeLClientRxLen);
				}
				else
				{
					/* No Acion */
				}
			}
			else 
			{
				/* No Action */
			}

		}
		break;

		case TELINK_MQTT_DISCONNECTED:
		{
			if(1 == u8_gTeLinkConnStatus)
			{
				ST_MQTT_CONNECT_CONFIG_t st_gMqttConfigParams = 
				{
					.s32_mSocketNumber = s32_gTeLinkSocketId,
					.au8_mClientId      = "TELINK4G2000005",
					.au8_mUserName        = "",
					.au8_mPassword      = "",
				};

				/* Connect to Mqtt server */
				if(MQTT_Connect(&st_gMqttConfigParams) > 0) 
				{
					/* Subscribe to sub topic */
					if(MQTT_Subscribe(s32_gTeLinkSocketId, ps8_gSubTopic) > 0)
					{
#ifdef __DEBUG
						printf("Connected & Subscribed to %s\r\n", ps8_gSubTopic);
#endif

						MQTT_Subscribe(s32_gTeLinkSocketId, ps8_gUpdateTopic);
		
						/* update Conn mgr state */
						en_gTeLinkState = TELINK_MQTT_CONNECTED;
						u8_gWClientConnStatus = 1;
						TeLClient_SendCommand(s32_gTeLinkSocketId, 0x51, 0x00, "CONNECTED", strlen("CONNECTED"));
					}
					else
					{
#ifdef __DEBUG
						printf("Connected but could not Subscribe to %s\r\n", ps8_gSubTopic);
#endif
					}
				}
				else
				{
#ifdef __DEBUG
					printf("Mqtt server Connection failed\r\n");
#endif
					en_gTeLinkState = TELINK_MQTT_INIT;
				}
			}
			else
			{
				/* No Action */
			}
		}
		break;

		default:
		break;
		}

		usleep(500000);
	}
}

/* TeLClient initialization */
void TELINK_Init(void)
{
	pthread_attr_t pTeLinkAttr;
	
	/* Create a Queue for TeLink Data Handling */
	TeLinkMsgQueueKey = TELINK_MSG_QUEUE_KEY;
   	TeLinkMsgQFlags = (IPC_CREAT | 0777);
	TeLinkMsgQueueKey = ftok(TELINK_QUEUE_PATH_NAME, TELINK_PROJECT_ID);

	if((TeLinkMsgQueueID = msgget(TeLinkMsgQueueKey, TeLinkMsgQFlags)) < 0)
	{
		perror("CLIENT: msgget");
		exit(EXIT_FAILURE);
	}

	TeLClient_GetSocketInfo(ps8_gPubTopic);
	TeLClient_RegisterHandler(0x41, 0x00, TELINK_PingHandler);

	/* Create TELINK thread */
	pthread_attr_init(&pTeLinkAttr);
	pthread_attr_setdetachstate(&pTeLinkAttr, PTHREAD_CREATE_DETACHED);
	pthread_create(&pTeLinkHandle, &pTeLinkAttr, pTELINK_ConnMgrThread, NULL);
}

void TELINK_PingHandler(INT8U u8_fDid, INT8U *pu8_fData, INT16U u16_fLength)
{
	TeLClient_SendCommand(s32_gTeLinkSocketId, 0x41, 0x00, "TECHM_APP_OK", strlen("TECHM_APP_OK"));
}
