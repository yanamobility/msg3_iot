/**************************************************************************************************/
/*                                                                                                */
/*                          Think Embedded Pvt. Ltd. (TEPL) Design Unit                           */
/*                                                                                                */
/**************************************************************************************************/
/*                                                                                                */
/* All Rights Reserved Copyright (C) 2018, Think Embedded Pvt. Ltd. (TEPL)                        */
/*                                                                                                */
/* Redistribution and use in source and binary forms, with or without modification,               */
/* are not permitted in any case                                                                  */
/*                                                                                                */
/* TEPL's name may not be used to endorse or promote products derived from this software without  */
/* specific prior written permission.                                                             */
/*                                                                                                */
/**************************************************************************************************/

/**************************************************************************************************/
/*  File Name   : TeLINK_public.h                                                                 */
/*  Author      : Naga Prasad                                                                     */
/*  Version 	: v2.0                                                                            */
/*  Date        : 04-07-2019                                                                      */
/*                                                                                                */
/* @Description : TeLINK Frame Work                                                               */
/**************************************************************************************************/

#ifndef _TELINK_PUBLIC_H_
#define _TELINK_PUBLIC_H_

/**************************************************************************************************/
/* System Header Files Section                                                                    */
/**************************************************************************************************/

/**************************************************************************************************/
/* User Header Files Section                                                                      */
/**************************************************************************************************/

#include "Includes.h"

/**************************************************************************************************/
/* Export Global Variables Section                                                                */
/**************************************************************************************************/

extern int s32_gTeLinkSocketId;
extern INT8U au8_gSerialNo[16];

extern INT8U au8_gNmeaPacket[128];
/**************************************************************************************************/
/* Export Global Definitions Section                                                              */
/**************************************************************************************************/

/* Enum for Connection Management states */
typedef enum
{
	TELINK_MQTT_INIT = 0,
	TELINK_MQTT_CONNECTED,
	TELINK_MQTT_DISCONNECTED

}EN_TELINK_STATES_t;

typedef struct
{
	long Msg_type;
	int MsgLength;
	uint8_t QueueBuff[512];
	
}ST_TELINK_MSG_QUEUE_t;

/**************************************************************************************************/
/* Export Functions Section                                                                       */
/**************************************************************************************************/

extern void TeLINK_Init(void);

extern void TELINK_ConnectedCb(INT32S s32_fCsq);

extern void TELINK_DisConnectedCb(INT32S s32_fCsq);

extern void TELINK_DataCallBack(INT8S *ps8_fData, INT16U u16_fLength);

#endif

/**************************************************************************************************/
/* End of  TeLINK_public.h                                                                        */
/**************************************************************************************************/
