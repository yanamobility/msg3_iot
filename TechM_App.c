#include <stdio.h>
#include <time.h>
#include <pthread.h>
#include <string.h>
#include <stdlib.h>

#include "ql_uart.h"
#include "ql_oe.h"
#include "ql_gpio.h"
#include "ql_wwan_v2.h"

#include "MQTT/MQTT.h"
#include "TBUS/TBUS.h"

#include "TeLink/TeLink.h"
#include "WatsonClient/WatsonClient.h"
#include "TeLClient/TeLClient.h"
#include "TechM_App.h"

//#define __DEBUG 1

/* */
#define TECHM_MODEM_READY_NOTIFICATION	( 0x41 )
#define TECHM_SNF_RECORD				( 0x43 )
#define TECHM_CONNECTION_STATUS_CMD		( 0x44 )
#define TECHM_POWER_DOWN_CMD			( 0x39 )

static void data_call_state_callback(ql_data_call_state_s *state);

pthread_t pTELCConnectivityHandle;
pthread_t pTELCFileTestHandle;

static Enum_PinName m_GpioPin = PINNAME_PMU_GPIO1;

uint8_t au8_GpsDataPacket[1024] = {0,};
INT8S au8_gJsonData[512] = {0, };

ql_data_call_s data_call[1];
ql_data_call_info_s data_call_info;
ql_data_call_error_e err = QL_DATA_CALL_ERROR_NONE;

/* */
INT32U u32_gRecordsAvail = 0;
INT32S s32_gSignalStrength = 0;

static bool g_cb_recv = FALSE;
static INT8U NetworkStatus = 0;

INT8U u8_gIsHostReady = 0;

/* */
volatile EN_TECHM_STATES_t en_gTechmState = TECHM_SLEEP;

/* */
static ql_data_call_s g_call = 
{
	.profile_idx = 1,
	.reconnect = true,
	.ip_family = QL_DATA_CALL_TYPE_IPV4,
};

double f64_gLattitde = 0;
INT8S s8_gLatDir = 'S';
double f64_gLongitude = 0;
INT8S s8_gLongDir = 'E';
float f32_gHeading = 0;
INT8U u8_gGpsLatch = 'V';

uint64_t u64_gTimeStamp = 0;

/* */
typedef struct
{
	INT32U u32_mYear;
	INT8U u8_mMonth;
	INT8U u8_mDay;
	INT8U u8_mHours;
	INT8U u8_mMinutes;
	INT8U u8_mSeconds;
     
}ST_DATE_TIME_t;

/* */
ST_DATE_TIME_t st_gDate;

/**************************************************************************************************/
/* Function Name   :                                                                              */
/*                                                                                                */
/* Description     :                                                                              */
/*                                                                                                */
/* In Params       :                                                                              */
/*                                                                                                */
/* Out Params      :                                                                              */
/*                                                                                                */
/* Return Value    :                                                                              */
/**************************************************************************************************/

INT64U DATE2SEC_GetSeconds(const ST_DATE_TIME_t *st_fDate)
 {
    INT32U u32_lYear = 0;
    INT32U u32_lMonth = 0;
    INT32U u32_lDay = 0;
    INT64U u64_lTime = 0;
 
    /* Copy Year, Month, Day */
    u32_lYear = st_fDate->u32_mYear;
    u32_lMonth = st_fDate->u8_mMonth;
    u32_lDay = st_fDate->u8_mDay;
 
    /* January and February are counted as months 13 and 14 of the previous year */
    if(u32_lMonth <= 2)
    {
       u32_lMonth += 12;
       u32_lYear -= 1;
    }
 
    /* Convert years to days */
    u64_lTime = (365 * u32_lYear) + (u32_lYear / 4) - (u32_lYear / 100) + (u32_lYear / 400);
    
    /* Convert months to days */
    u64_lTime += (30 * u32_lMonth) + (3 * (u32_lMonth + 1) / 5) + u32_lDay;
    
    /* Unix time starts on January 1st, 1970 */
    u64_lTime -= 719561;
    
    /* Convert days to seconds */
    u64_lTime *= 86400;
    
    /* Add hours, minutes and seconds */
    u64_lTime += (3600 * st_fDate->u8_mHours) + (60 * st_fDate->u8_mMinutes) + \
                                                                    st_fDate->u8_mSeconds;
 
    /* Return Unix time */
    return u64_lTime;
}

/* GPRMC Sample Data format */
//$GPRMC,081836,A,3751.65,S,14507.36,E,000.0,360.0,130998,011.3,E*62
/* */
void GPRMC_Parser(uint8_t *pu8_fData)
{
	INT16U u16_lMilliSec = 0;
	uint8_t au8_lTemp[20] = {0, };
	uint8_t u8_lIndex = 0;

	/* Skip GPRMC */
	while(',' != *pu8_fData++);
    
        if(*pu8_fData != ',')
        {
            /* Parse Time */
            memset(au8_lTemp, 0, sizeof(au8_lTemp));
            
            /* Parse Hourse */
            au8_lTemp[0] = *pu8_fData++;
            au8_lTemp[1] = *pu8_fData++;
            st_gDate.u8_mHours = atoi((const char *)au8_lTemp);
            
            /* Parse Minutes */
            au8_lTemp[0] = *pu8_fData++;
            au8_lTemp[1] = *pu8_fData++;
            st_gDate.u8_mMinutes = atoi((const char *)au8_lTemp);

            /* Parse Secounds */
            au8_lTemp[0] = *pu8_fData++;
            au8_lTemp[1] = *pu8_fData++;
            st_gDate.u8_mSeconds = atoi((const char *)au8_lTemp);  

            pu8_fData++;
            
            /* Parse Time in Milli secounds */
            memset(au8_lTemp, 0, sizeof(au8_lTemp));
            while(',' != *pu8_fData)
            {
                u8_lIndex = u8_lIndex % 15;
                au8_lTemp[u8_lIndex++] = *pu8_fData++;
            }

            /* Get Milli Secounds */
            u16_lMilliSec = atoi((const char *)au8_lTemp);
            
            pu8_fData++;
            
            /* Parse Gps Status */
            u8_gGpsLatch = *pu8_fData++;
        }
        else
        {
            u8_gGpsLatch = 'V';
        }
        
        if('A' == u8_gGpsLatch)
        {
            pu8_fData++;

            /* Parse lattitude */
            memset(au8_lTemp, 0, sizeof(au8_lTemp));

            au8_lTemp[0] = *pu8_fData++;
            au8_lTemp[1] = *pu8_fData++;
            
            f64_gLattitde = (double)atoi((const char *)au8_lTemp);
            
            memset(au8_lTemp, 0, sizeof(au8_lTemp));
            u8_lIndex = 0;
            
            while(',' != *pu8_fData)
            {
                au8_lTemp[u8_lIndex++] = *pu8_fData++;
            }
            
            f64_gLattitde += (double)atof((const char *)au8_lTemp)/60;
            
            pu8_fData++;
        
            while(',' != *pu8_fData++);
            
            /* Parse longitude */
            memset(au8_lTemp, 0, sizeof(au8_lTemp));

            au8_lTemp[0] = *pu8_fData++;
            au8_lTemp[1] = *pu8_fData++;
            au8_lTemp[2] = *pu8_fData++;
            
            f64_gLongitude = (double)atoi((const char *)au8_lTemp);
            
            memset(au8_lTemp, 0, sizeof(au8_lTemp));
            
            u8_lIndex = 0;
            while(',' != *pu8_fData)
            {
                au8_lTemp[u8_lIndex++] = *pu8_fData++;
            }
            
            f64_gLongitude += (double)atof((const char *)au8_lTemp)/60;
            
            pu8_fData++;
            
            while(',' != *pu8_fData++);
            while(',' != *pu8_fData++);
            
            /* Parse Direction */
            memset(au8_lTemp, 0, sizeof(au8_lTemp));
            u8_lIndex = 0;

            while(',' != *pu8_fData)
            {
                au8_lTemp[u8_lIndex++] = *pu8_fData++;
            }
            
            f32_gHeading = atof((const char *)au8_lTemp);
            
            pu8_fData++;
            
            /* Parse Date */
            memset(au8_lTemp, 0, sizeof(au8_lTemp));
            
            /* Parse day */
            au8_lTemp[0] = *pu8_fData++;
            au8_lTemp[1] = *pu8_fData++;
            st_gDate.u8_mDay = atoi((const char *)au8_lTemp);
            
            /* Parse Month */
            au8_lTemp[0] = *pu8_fData++;
            au8_lTemp[1] = *pu8_fData++;
            st_gDate.u8_mMonth = atoi((const char *)au8_lTemp);
 
            /* Parse Year */
            au8_lTemp[0] = *pu8_fData++;
            au8_lTemp[1] = *pu8_fData++;
            st_gDate.u32_mYear = 2000 + atoi((const char *)au8_lTemp); 
            
            /* Convert Time in to milli secounds */
            u64_gTimeStamp = (DATE2SEC_GetSeconds(&st_gDate) * 1000) + u16_lMilliSec;
	}
	else
	{
		/* No Action */
	} 
}

static void ql_loc_rx_ind_msg_cb(loc_client_handle_type  h_loc,
                                 E_QL_LOC_NFY_MSG_ID_T   e_msg_id,
                                 void                    *pv_data,
                                 void                    *contextPtr)
{
    switch(e_msg_id)
    {
        case E_QL_LOC_NFY_MSG_ID_STATUS_INFO:
            break;
        case E_QL_LOC_NFY_MSG_ID_LOCATION_INFO:
        {
            QL_LOC_LOCATION_INFO_T *pt_location = (QL_LOC_LOCATION_INFO_T *)pv_data;
            break;
        }
        case E_QL_LOC_NFY_MSG_ID_SV_INFO:
            break;
        case E_QL_LOC_NFY_MSG_ID_NMEA_INFO:
        {
            QL_LOC_NMEA_INFO_T  *pt_nmea = (QL_LOC_NMEA_INFO_T  *)pv_data;
#ifdef __DEBUG
            printf("NMEA info: timestamp=%lld, length=%d, nmea=%s\n", 
                    pt_nmea->timestamp, pt_nmea->length, pt_nmea->nmea);
#endif
			if(strstr(pt_nmea->nmea, "$GPRMC"))
			{
				GPRMC_Parser(pt_nmea->nmea);
			}
 
            break;
        }
        case E_QL_LOC_NFY_MSG_ID_CAPABILITIES_INFO:
            break;
        case E_QL_LOC_NFY_MSG_ID_AGPS_STATUS:
            break;
        case E_QL_LOC_NFY_MSG_ID_NI_NOTIFICATION:
            break;
        case E_QL_LOC_NFY_MSG_ID_XTRA_REPORT_SERVER:
            break;
    }
}

void GpsInfo_Init(void)
{
    int                     ret         = E_QL_OK;
    int                     h_loc       = 0;
    int                     bitmask     = 0;
    QL_LOC_POS_MODE_INFO_T  t_mode      = {0};
    QL_LOC_LOCATION_INFO_T  t_loc_info  = {0};

    ret = QL_LOC_Client_Init(&h_loc);
#ifdef __DEBUG
    printf("QL_LOC_Client_Init ret %d with h_loc=%d\n", ret, h_loc);
#endif
    
    ret = QL_LOC_AddRxIndMsgHandler(ql_loc_rx_ind_msg_cb, (void*)h_loc); 
#ifdef __DEBUG 
    printf("QL_LOC_AddRxIndMsgHandler ret %d\n", ret); 
#endif
        
    bitmask = 511;

    /* Set what we want callbacks for */
    ret = QL_LOC_Set_Indications(h_loc, bitmask);
#ifdef __DEBUG 
    printf("QL_LOC_Set_Indications ret %d\n", ret);
#endif

    t_mode.mode                 = E_QL_LOC_POS_MODE_STANDALONE;
    t_mode.recurrence           = E_QL_LOC_POS_RECURRENCE_PERIODIC;
    t_mode.min_interval         = 1000;  //report nmea frequency 1Hz
    t_mode.preferred_accuracy   = 50;    // <50m
    t_mode.preferred_time       = 90;    // 90s
    ret = QL_LOC_Set_Position_Mode(h_loc, &t_mode);
#ifdef __DEBUG 
    printf("QL_LOC_Set_Position_Mode ret %d\n", ret);
#endif
    
    ret = QL_LOC_Start_Navigation(h_loc);
}

static void data_call_state_callback(ql_data_call_state_s *state)
{
	if(g_call.profile_idx == state->profile_idx)
	{
		if(state->state == QL_DATA_CALL_CONNECTED)
		{
			if(state->ip_family == QL_DATA_CALL_TYPE_IPV4)
			{
#ifdef __DEBUG
				fprintf(stdout, "\tIP address:          %s\n", inet_ntoa(state->v4.ip));
				fprintf(stdout, "\tGateway address:     %s\n", inet_ntoa(state->v4.gateway));
				fprintf(stdout, "\tPrimary DNS address: %s\n", inet_ntoa(state->v4.pri_dns));
				fprintf(stdout, "\tSecond DNS address:  %s\n", inet_ntoa(state->v4.sec_dns));
#endif
			}

			NetworkStatus = 1;
		}
		else
		{
			NetworkStatus = 0;
		}
	}
	return;
}

void QOPl_DataCallInit(void)
{
	ql_data_call_info_s info = {0};
	ql_data_call_error_e err = QL_DATA_CALL_ERROR_NONE;
	int ret = 0;

	ret = ql_data_call_init(data_call_state_callback);
	if(ret)
	{
		TBUS_SendCommand(0x38, 0x00, NULL, 0);
		Ql_Powerdown(1);
		fprintf(stdout, "ql_data_call_init failed.ret=%d\n", ret);
	}

	ret = ql_data_call_info_get(g_call.profile_idx, g_call.ip_family, &info, &err);
	if(ret)
	{
		TBUS_SendCommand(0x38, 0x00, NULL, 0);
		Ql_Powerdown(1);
#ifdef __DEBUG
		printf("get profile index %d information failure: errno 0x%x\n", g_call.profile_idx, err);
#endif
	}
	else
	{
		if(QL_DATA_CALL_CONNECTED == info.v4.state)
		{
#ifdef __DEBUG
			printf("the profile index %d is already connected, don't up\n", g_call.profile_idx);
#endif
			NetworkStatus = 1;
		}
		else
		{
			/* No Action */
		}
	}

	ret = ql_data_call_start(&g_call, &err);
	if(ret)
	{
		TBUS_SendCommand(0x38, 0x00, NULL, 0);
		Ql_Powerdown(1);
#ifdef __DEBUG
		printf("the profile index %d start data call failure: 0x%x\n", g_call.profile_idx, err);
#endif
	}

#ifdef __DEBUG
	printf("the profile index %d start data call success\n", g_call.profile_idx);
#endif
    usleep(500*1000);
}

/* Handler for Techm Ping */
void TECHM_ModemReadyRespHandler(INT8U u8_fDID, INT8U * p_fPayLoad, INT16U u16_fPayLoadLength)
{
	if(0x06 == p_fPayLoad[0])
	{
		u8_gIsHostReady = 1;
	}
}

/* Handler to Read record from Aux controller */
void TECHM_SnfRecordHandler(INT8U u8_fDID, INT8U * p_fPayLoad, INT16U u16_fPayLoadLength)
{
	INT64U u64_lTimeStamp = 0;
	double f64_lLattitude = 0.0;
	double f64_lLongitude = 0.0;
	INT32F f32_lHeading = 0.0;
	INT8U  u8_lSpeed = 0;
	INT16U u16_lRpm = 0;
	INT8U u8_lIgnitionStatus = 0;
	INT8U au8_lIgnBuf[4] = {0,};
	struct tm *timeinfo;
	INT8U u8_lSnfRecordCount = 0;
	INT8U u8_lIndex = 0;
	INT8U u8_lRecordLength = 0;
	INT8U au8_lTempBuf[256] = {0,};
	INT8U u8_lAck = 0x06;
	INT8U u8_lSendIgnStatus = 0;
	INT8U u8_lDevicePlugInStatus = 0;

	/* Read Snf Record Count From Received Data */
	COMMON_PAN2Byte(p_fPayLoad, &u8_lSnfRecordCount);

	p_fPayLoad = p_fPayLoad + 2;

	u8_gPrevSendSuccess = 0;

	/* Read All Snf Records */
	for(u8_lIndex = 0; u8_lIndex < u8_lSnfRecordCount; u8_lIndex++)
	{
		/* Read Record Length */
		COMMON_PAN2Byte(p_fPayLoad, &u8_lRecordLength);

		p_fPayLoad = p_fPayLoad + 2;

		memset(au8_lTempBuf, 0x00, sizeof(au8_lTempBuf));
		memcpy(au8_lTempBuf, p_fPayLoad, u8_lRecordLength);

		/* read Record */
		sscanf(au8_lTempBuf, "%llu,%lf,%lf,%f,%hhd,%hd,%hhd,%hhd,%hhd", &u64_lTimeStamp, &f64_lLattitude, &f64_lLongitude, \
					 							&f32_lHeading, &u8_lSpeed, &u16_lRpm, \
														&u8_lIgnitionStatus,&u8_lSendIgnStatus, &u8_lDevicePlugInStatus);
	
		p_fPayLoad = p_fPayLoad + u8_lRecordLength;

		if(1 == u8_lSendIgnStatus)
		{
			u8_lSendIgnStatus = 0;
			memset(au8_lIgnBuf, 0, sizeof(au8_lIgnBuf));
			if(1 == u8_lIgnitionStatus)
			{
				strcpy(au8_lIgnBuf, "on");
			}
			else
			{
				strcpy(au8_lIgnBuf, "off");
			}

			/* Prepare Json Packet */
			memset(au8_gJsonData, 0, sizeof(au8_gJsonData));
			sprintf(au8_gJsonData, "{\"vid\": \"%s\",\"time\": \"%llu\",\"lat\": \"%.12lf\",\"lon\": \"%.12lf\",\"heading\": \"%.6f\",\"speed\": \"%d\",\"props\": {\"engineRPM\": \"%d\", \"fuelLevel\": \"0\",\"ignition\": \"%s\"}}", TECHM_DEVICE3_VID, u64_lTimeStamp, f64_lLattitude, f64_lLongitude, f32_lHeading,u8_lSpeed, u16_lRpm, au8_lIgnBuf);
		}
		else
		{
			/* Prepare Json Packet */
			memset(au8_gJsonData, 0, sizeof(au8_gJsonData));
			sprintf(au8_gJsonData, "{\"vid\": \"%s\",\"time\": \"%llu\",\"lat\": \"%.12lf\",\"lon\": \"%.12lf\",\"heading\": \"%.6f\",\"speed\": \"%d\",\"props\": {\"engineRPM\": \"%d\", \"fuelLevel\": \"0\"}}", TECHM_DEVICE3_VID, u64_lTimeStamp, f64_lLattitude, f64_lLongitude, f32_lHeading,u8_lSpeed, u16_lRpm);
		}

		/* Send Json Data to Clients */
		TELINK_DataCallBack(au8_gJsonData, strlen(au8_gJsonData));
		WCLIENT_DataCallBack(au8_gJsonData, strlen(au8_gJsonData));

		/* Send Ack To Host Controller */
		TBUS_SendCommand(0x43, 0x00, &u8_lAck, 1);
		
		Ql_GPIO_SetLevel(m_GpioPin, PINLEVEL_HIGH);
		usleep(50000);
		Ql_GPIO_SetLevel(m_GpioPin, PINLEVEL_LOW);
	}
}

/* Handler to Write record to Aux controller */
void TECHM_ConnectionStatusHandler(INT8U u8_fDID, INT8U * p_fPayLoad, INT16U u16_fPayLoadLength)
{
	p_fPayLoad[u16_fPayLoadLength] = 0;

	memset(au8_GpsDataPacket, 0x00, sizeof(au8_GpsDataPacket));
	
	sprintf(au8_GpsDataPacket, "%llu,%.12lf,%.12lf,%.6f,%d,%hhd,%hhd,%hhd,%c", u64_gTimeStamp, f64_gLattitde, f64_gLongitude, \
						f32_gHeading, s32_gSignalStrength, NetworkStatus, u8_gWClientConnStatus, u8_gPrevSendSuccess, u8_gGpsLatch);


	TBUS_SendCommand(TECHM_CONNECTION_STATUS_CMD, 0x00, au8_GpsDataPacket, strlen(au8_GpsDataPacket));
}

/* This thread monitor the data connection status & reports to respective clients */
void *pCellularConnectionTask(void * vParameter)
{
	unsigned long long int u64_lTimeStamp = 0;
	INT8U u8_lDebounceFlag = 0;
	FILE* FilePtr = NULL;
	INT8U u8_lErrorIndex = 0;
	ql_data_call_info_s info = {0};
	ql_data_call_error_e err = QL_DATA_CALL_ERROR_NONE;

	/* Data call */
	QOPl_DataCallInit();

	GpsInfo_Init();

	for(;;)
	{
		if(0 == u8_gIsHostReady)
		{	
			/* Ping the Auxalary controller */
			TBUS_SendCommand(TECHM_MODEM_READY_NOTIFICATION, 0x00, NULL, 0);
		}
		
		/* Get Signal Strength */
		QL_NW_GetCSQ(&s32_gSignalStrength);
#ifdef __DEBUG
		printf("Signal Strength: %d\n", s32_gSignalStrength);
#endif
		/* Get Data connection status */
		if(ql_data_call_info_get(g_call.profile_idx, g_call.ip_family, &info, &err)) 
		{
#ifdef __DEBUG
			printf("get profile index %d information failure: errno 0x%x\n", g_call.profile_idx, err);
#endif
			TBUS_SendCommand(0x38, 0x00, NULL, 0);
			Ql_Powerdown(1);
		}

		/* Check for Data connection */
		if(QL_DATA_CALL_CONNECTED == info.v4.state) 
		{
			NetworkStatus = 1;
		}
		else
		{
			NetworkStatus = 0;
		}

		if(1 == NetworkStatus)
		{
			TELINK_ConnectedCb(s32_gSignalStrength);
			WCLIENT_ConnectedCb(s32_gSignalStrength);
		}
		else
		{
			TELINK_DisconnectedCb(s32_gSignalStrength);
			WCLIENT_DisconnectedCb(s32_gSignalStrength);
		}

		sleep(1);
	}

	
}

void TECHM_GpsStatusHandler(INT8U u8_fDID, INT8U * p_fPayLoad, INT16U u16_fPayLoadLength)
{
	if('A' == u8_gGpsLatch)
	{
		TeLClient_SendCommand(s32_gTeLinkSocketId, 0x50, 0x01, NULL, 0);
	}
	else
	{
		TeLClient_SendCommand(s32_gTeLinkSocketId, 0x50, 0x00, NULL, 0);
	}
}

void TECHM_PowerDownHandler(INT8U u8_fDID, INT8U * p_fPayLoad, INT16U u16_fPayLoadLength)
{
	INT8U u8_lAck = TBUS_ACK;

	if(0x00 == u8_fDID)
	{
		TBUS_SendCommand(TECHM_POWER_DOWN_CMD, 0x00, &u8_lAck, 1);

		/* Power Down Modem */
		Ql_Powerdown(0);
	}
}

int main(void)
{
	pthread_attr_t pAttr;

	/* Init Tbus */
	TBUS_Init();

	/* Register TechM handlers to Tbus */
	TBUS_RegisterHandler(TECHM_MODEM_READY_NOTIFICATION, 0x00, TECHM_ModemReadyRespHandler);
	TBUS_RegisterHandler(TECHM_SNF_RECORD, 0x00, TECHM_SnfRecordHandler);
	TBUS_RegisterHandler(TECHM_CONNECTION_STATUS_CMD, 0x00, TECHM_ConnectionStatusHandler);
	TBUS_RegisterHandler(TECHM_POWER_DOWN_CMD, 0x00, TECHM_PowerDownHandler);

	TeLClient_RegisterHandler(0x50, 0x00, TECHM_GpsStatusHandler);

	/* Create Connectivity Task */
	pthread_attr_init(&pAttr);
	pthread_attr_setdetachstate(&pAttr, PTHREAD_CREATE_DETACHED);
    pthread_create(&pTELCConnectivityHandle, &pAttr, pCellularConnectionTask, NULL);

	/* Init watson Server connection */
	//WCLIENT_Init();
	
	/* Init Telink server connection */
	TELINK_Init();

	/* Init TelClient */
	TeLClient_Init();
	
	while(1);

	return 0;
}

