#include <stdio.h>
#include <string.h>

#include <pthread.h>
#include <sys/types.h> 
#include <netinet/in.h> 
#include <openssl/ssl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#include "Includes.h"

#include "MQTT/MQTT.h"
#include "WatsonClient/WatsonClient.h"

#define TECHM_TRANSMIT_SUCCESS_NOFITICATION			( 0x42 )

#define MSG_QUEUE_KEY      (key_t)100

/* Parameters To get Key For Message Queue*/
#define QUEUE_PATH_NAME "/tmp/msgkey"
#define PROJECT_ID 'R'

/* Queue for TeLClient Service Listner */
int WClientMsgQueueID = 0;
ssize_t MsgQBytes = 0;
key_t MsgQueueKey = 0;
int MsgQFlags = 0;

volatile INT8U u8_gPrevSendSuccess = 1;

/* */
pthread_t pWClientHandle;

/* Connection manager state variable */
EN_WCLIENT_STATES_t en_gWClientState = WCLIENT_MQTT_INIT;

volatile INT8U u8_gWClientConnStatus = 0;
SSL *pst_gWClientSockFd;
INT32S s32_gWClientStatus = -1;

INT32S s32_gWClientCsq = 0;
INT8U u8_gWClientDataStatus = 0;

/* */
void WCLIENT_ConnectedCb(INT32S s32_fCsq)
{
	u8_gWClientDataStatus = 1;
	s32_gWClientCsq = s32_fCsq;
}

/* */
void WCLIENT_DisconnectedCb(INT32S s32_fCsq)
{
	s32_gWClientCsq = s32_fCsq;

	u8_gWClientDataStatus = 0;
	u8_gWClientConnStatus = 0;
	en_gWClientState = WCLIENT_MQTT_INIT;
}

/* */
void WCLIENT_DataCallBack(INT8S *ps8_fData, INT16U u16_fLength)
{
	ST_MSG_QUEUE_t st_lTxMsgQueue;

	//printf("%s\n", ps8_fData);

	memset(&st_lTxMsgQueue, 0x00, sizeof(st_lTxMsgQueue));

	st_lTxMsgQueue.Msg_type = 2;
	memcpy(st_lTxMsgQueue.QueueBuff, ps8_fData, u16_fLength);
	st_lTxMsgQueue.MsgLength = u16_fLength;

	msgsnd(WClientMsgQueueID, &st_lTxMsgQueue, sizeof(st_lTxMsgQueue), 0);
}

/* TeLClient thread */
void *pWCLIENT_ConnMgrThread(void *vp_fParams)
{
	INT8U u8_lSendAckBuff[128] = {0,};

	ST_MSG_QUEUE_t st_lRxMsgQueue;

	st_lRxMsgQueue.Msg_type = 2;

	for(;;)
	{
		switch(en_gWClientState)
		{
		case WCLIENT_MQTT_INIT:
		{
			if((1 == u8_gWClientDataStatus) && (s32_gWClientCsq >= 9) && (s32_gWClientCsq <= 31))
			{
				/* Open Tcp socket for Mqtt connection */
				s32_gWClientStatus = MQTT_SSLInit(&pst_gWClientSockFd, \
									"trbqxm.messaging.internetofthings.ibmcloud.com", 8883);

				if(-1 == s32_gWClientStatus)
				{
#ifdef __DEBUG
					printf("Sll Socket not Created\r\n");
#endif
					sleep(5);
				}
				else
				{
#ifdef __DEBUG
					printf("Ssl Socket Created\r\n");
#endif

					/* update Conn mgr state */
					en_gWClientState = WCLIENT_MQTT_DISCONNECTED;
				}
			}
			else
			{
				/* No Action */
			}
		}
		break;

		case WCLIENT_MQTT_CONNECTED:
		{
			/* Read All Available Messages & */
			while(1)
			{
				MsgQBytes = msgrcv(WClientMsgQueueID, &st_lRxMsgQueue, sizeof(st_lRxMsgQueue), st_lRxMsgQueue.Msg_type, IPC_NOWAIT);

				if(-1 == MsgQBytes)
				{
					if(0 == u8_gPrevSendSuccess)
					{
						//printf("+++++++++++++++WatsonSend Successful\n");
						u8_gPrevSendSuccess = 1;
					}
					break;
				}
				else
				{	
					/* pubish data to mqtt server */
					if(MQTT_SslPublish(pst_gWClientSockFd, "iot-2/evt/probe-data-zone1/fmt/json", \
													st_lRxMsgQueue.QueueBuff, st_lRxMsgQueue.MsgLength) > 0)
					{
#ifdef __DEBUG
						printf("publishing to Watson Server successful\r\n");
#endif 
					}
					else
					{
#ifdef __DEBUG
						printf("publishing to Watson Server failed\r\n");
#endif
					}

					usleep(250000);
				}
			}						
		}
		break;

		case WCLIENT_MQTT_DISCONNECTED:
		{
			if(1 == u8_gWClientDataStatus)
			{
				ST_MQTT_CONNECT_CONFIG_t st_gWatsonCloudData =
				{
					.au8_mClientId    = "d:trbqxm:TELINK4G_OBD:TELINK4G2000003",
					.au8_mUserName    = "use-token-auth",
					.au8_mPassword    = TECHM_DEVICE3_SEQURITY,
				};

				/* Connect to Mqtt server */
				if(MQTT_SSlConnect(pst_gWClientSockFd, &st_gWatsonCloudData) > 0) 
				{
#ifdef __DEBUG
					printf("Connected to Watson cloud\r\n");
#endif
					
					/* update Conn mgr state */
					en_gWClientState = WCLIENT_MQTT_CONNECTED;
					u8_gWClientConnStatus = 1;
				}
				else
				{
#ifdef __DEBUG
					printf("Not Connected to Watson cloud\r\n");
#endif
				}
			}			
		}
		break;

		default:
		break;
		}

		sleep(1);
	}
}

/* TeLClient initialization */
void WCLIENT_Init(void)
{
	pthread_attr_t pWClientAttr;
	
	/* Create a Queue for WatsonClient Data Handling */
	MsgQueueKey = MSG_QUEUE_KEY;
   	MsgQFlags = (IPC_CREAT | 0777);
	MsgQueueKey = ftok(QUEUE_PATH_NAME, PROJECT_ID);

	if((WClientMsgQueueID = msgget(MsgQueueKey, MsgQFlags)) < 0)
	{
		perror("CLIENT: msgget");
		exit(EXIT_FAILURE);
	}

	/* Create TeLClient thread */
	pthread_attr_init(&pWClientAttr);
	pthread_attr_setdetachstate(&pWClientAttr, PTHREAD_CREATE_DETACHED);

	pthread_create(&pWClientHandle, &pWClientAttr, pWCLIENT_ConnMgrThread, NULL);
}
