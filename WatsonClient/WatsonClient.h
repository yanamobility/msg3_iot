#ifndef _WATSON_CLIENT_H_
#define _WATSON_CLIENT_H_

#define TECHM_DEVICE1_VID           "HR29Q4700"
#define TECHM_DEVICE2_VID           "DL7CN1690"
#define TECHM_DEVICE3_VID           "KA01ER7013"

/* */
#define TECHM_DEVICE1_SEQURITY           "mJeTbcXV3PdZD7GSky"
#define TECHM_DEVICE2_SEQURITY           "+pZeAaiT8T47Ui!Cws"
#define TECHM_DEVICE3_SEQURITY           "sTy+RU!3I8sNEAVSTH"

/* Enum for conn mgr states */
typedef enum
{
	WCLIENT_MQTT_INIT = 0,
	WCLIENT_MQTT_CONNECTED,
	WCLIENT_MQTT_DISCONNECTED

}EN_WCLIENT_STATES_t;

typedef struct
{
	long Msg_type;
	int MsgLength;
	uint8_t QueueBuff[512];
	
}ST_MSG_QUEUE_t;

extern volatile INT8U u8_gPrevSendSuccess;

extern volatile INT8U u8_gWClientConnStatus;

extern void WCLIENT_ConnectedCb(INT32S s32_fCsq);
extern void WCLIENT_DisconnectedCb(INT32S s32_fCsq);
extern void WCLIENT_Init(void);

/* */
extern void WCLIENT_DataCallBack(INT8S *ps8_fData, INT16U u16_fLength);

#endif
